#!/bin/bash
# put to ~/.dwm/autostart.sh and chmod +x
# CC0 1.0

feh --bg-fill /home/tastyfish/Pictures/wallpaper.jpg

export _JAVA_AWT_WM_NONREPARENTING=1 # fix java

PROC_COUNT=`pgrep -x -c "autostart.sh"`

if [ $PROC_COUNT != "1" ] # do not run more instances
then
  exit 1
fi

while : 
do
  BAR_TIME=`date '+%d.%m.%y %H:%M'`
  BAR_AUDIO=`grep -oP "\d*%" <(amixer sget Master) | head -1`
  BAR_BATTERY=`acpi | awk -F", " '{ print $2 }'`

  BAR_FINAL="$BAR_TIME | A: $BAR_AUDIO | B: $BAR_BATTERY"

  xsetroot -name "$BAR_FINAL" 

  sleep 4
done
