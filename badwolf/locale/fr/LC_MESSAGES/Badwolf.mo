��    /      �  C           !        ;      Y     z     �     �     �  (   �       .     4   N  %   �  #   �     �  �   �     ^     f  ?   {  2   �  )   �  ?     5   X  C   �  :   �  _        m     u     �     �  !   �     �     �     �  #   	     1	     ?	     D	  =   H	  >   �	  5   �	      �	     
     3
  +   M
  2   y
  *   �
  v  �
  '   N  )   v  %   �  +   �     �       (   %  6   N      �  8   �  H   �  7   (  +   `  	   �  �   �     @     F  O   e  ?   �  ,   �  #   "  >   F  1   �  ;   �  _   �     S     a     y     �  &   �     �  $   �       2         S     a     f  =   j  E   �  >   �  *   -     X  *   x  8   �  >   �  2           (                
   )   %   -       /                                	          ,   #                 $             "            !              +                           .                   *             &                     '    %02i:%02i:%02i Download cancelled %02i:%02i:%02i Download error %02i:%02i:%02i Download finished %02i:%02i:%02i Downloading… Badwolf Downloads Bookmarks: Done.
 Bookmarks: Found %d bookmarks.
 Bookmarks: No loadable file found at %s
 Bookmarks: loading at %s
 Bookmarks: unable to create new XPath context
 Bookmarks: unable to evaluate XPath expression "%s"
 Bookmarks: unable to parse file "%s"
 Buildtime WebKit version: %d.%d.%d
 Continue Couldn't verify the TLS certificate to ensure a better security of the connection. You might want to verify your machine and network.

 Crashed Download starting… Error: Some unknown error occurred validating the certificate.
 Error: The X509 Certificate Authority is unknown.
 Error: The certificate has been revoked.
 Error: The certificate has expired. Check your system's clock.
 Error: The certificate is considered to be insecure.
 Error: The certificate isn't valid yet. Check your system's clock.
 Error: The given identity doesn't match the expected one.
 Minimalist and privacy-oriented web browser based on WebKitGTK
Runtime WebKit version: %d.%d.%d New tab Open new tab Out of Memory Running Badwolf version: %s
 Runtime WebKit version: %d.%d.%d
 TLS Error for %s. Temporarily Add Exception Toggle javascript Toggle loading images automatically Unknown Crash _IMG _JS badwolf: content-filter loaded, adding to content-manager…
 badwolf: failed to compile content-filters.json, err: [%d] %s
 badwolf: failed to load content-filter, err: [%d] %s
 content-filters file set to: %s
 search in current page the web process crashed.
 the web process exceeded the memory limit.
 the web process terminated for an unknown reason.
 webkit-web-extension directory set to: %s
 Project-Id-Version: Badwolf 0.3.0+gd88f2e7
Report-Msgid-Bugs-To: contact+badwolf-msgid@hacktivis.me
PO-Revision-Date: 2021-04-10 18:43+0200
Last-Translator: Haelwenn (lanodan) Monnier <contact@hacktivis.me>
Language-Team: French
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 %02i:%02i:%02i Téléchargement annulé %02i:%02i:%02i Erreur du téléchargement %02i:%02i:%02i Téléchargement finit %02i:%02i:%02i Téléchargement en cours… Téléchargements Badwolf Marque-Pages: Terminé.
 Marque-Pages: %d marque-pages trouvés.
 Marques-Pages: Aucun fichier chargeable trouvé à %s
 Marques-Pages: chargement à %s
 Marques-Pages: Échec à la création du contexte XPath
 Marques-Pages: Échec à l'évaluation de l'expression XPath « %s »
 Marques-Pages: Ëchec au parsage du fichier « %s »
 Version WebKit à la compilation: %d.%d.%d
 Continuer Impossibilité de vérifier le certificat TLS pour assurer une meilleure sécurité de la connection. Pensez potentiellement à vérifier votre machine et son réseau.

 Crash Démarrage du téléchargement Erreur : Une erreur inconnue est apparue pendant la validation du certificat.
 Erreur : L'autorité de certification (CA X509) est inconnue.
 Erreur : Le certificat à été révoqué.
 Erreur : Le certificat a expiré.
 Erreur : Le certificat est considéré comme non-sécurisé.
 Erreur : Le certificat n'est pas encore valide.
 Erreur : L'identité ne correspond pas à celle attendue.
 Navigateur WebKitGTK+ minimaliste et orienté vie privée
Version WebKit au lancement: %d.%d.%d Nouvel onglet Ouvrir un nouvel onglet Dépassement Mémoire Version de Badwolf: %s
 Version WebKit au lancement: %d.%d.%d
 Erreur TLS pour %s. Ajouter Temporairement une Exception (Dés)activer javascript (Dés)activer le chargement automatique des images Crash inconnu _IMG _JS badwolf: content-filter chargé, ajout à content-manager…
 badwolf: échec de compilation de content-filters.json, err: [%d] %s
 badwolf: échec de chargement de content-filter, err: [%d] %s
 Fichier content-filters configuré à: %s
 recherche dans la page courante le processus web a cessé de fonctionner.
 le processus web a dépassé la limitation de mémoire.
 le processus web s’est interrompu pour une raison inconnue.
 Répertoire webkit-web-extension configuré à %s
 