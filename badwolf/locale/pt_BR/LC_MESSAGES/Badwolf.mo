��    .      �  =   �      �  !   �           1     R     p     �     �  (   �     �  .   �  4   &  %   [  #   �     �  �   �     6     >  ?   S  2   �  )   �  ?   �  5   0  C   f  :   �     �     �     �       !   %     G     Y     s  #   �     �     �     �  =   �  >   �  5   =	      s	     �	     �	  +   �	  2   �	  *   $
  �  O
  '   �  ,     (   E     n     �     �  $   �  7   �       >   1  D   p  1   �     �  	     �        �     �  G   �  9   
  "   D  G   g  -   �  P   �  0   .     _     h     {     �     �     �     �     �  -        9     K     P  E   T  A   �  9   �  0        G     _  -   w  5   �  .   �     '      ,         -      %                       &                        
                   #              *                   !       .                 	              "      $      (   +   )                     %02i:%02i:%02i Download cancelled %02i:%02i:%02i Download error %02i:%02i:%02i Download finished %02i:%02i:%02i Downloading… Badwolf Downloads Bookmarks: Done.
 Bookmarks: Found %d bookmarks.
 Bookmarks: No loadable file found at %s
 Bookmarks: loading at %s
 Bookmarks: unable to create new XPath context
 Bookmarks: unable to evaluate XPath expression "%s"
 Bookmarks: unable to parse file "%s"
 Buildtime WebKit version: %d.%d.%d
 Continue Couldn't verify the TLS certificate to ensure a better security of the connection. You might want to verify your machine and network.

 Crashed Download starting… Error: Some unknown error occurred validating the certificate.
 Error: The X509 Certificate Authority is unknown.
 Error: The certificate has been revoked.
 Error: The certificate has expired. Check your system's clock.
 Error: The certificate is considered to be insecure.
 Error: The certificate isn't valid yet. Check your system's clock.
 Error: The given identity doesn't match the expected one.
 New tab Open new tab Out of Memory Running Badwolf version: %s
 Runtime WebKit version: %d.%d.%d
 TLS Error for %s. Temporarily Add Exception Toggle javascript Toggle loading images automatically Unknown Crash _IMG _JS badwolf: content-filter loaded, adding to content-manager…
 badwolf: failed to compile content-filters.json, err: [%d] %s
 badwolf: failed to load content-filter, err: [%d] %s
 content-filters file set to: %s
 search in current page the web process crashed.
 the web process exceeded the memory limit.
 the web process terminated for an unknown reason.
 webkit-web-extension directory set to: %s
 Project-Id-Version: Badwolf 0.4.0+g607300e
Report-Msgid-Bugs-To: contact+badwolf-msgid@hacktivis.me
PO-Revision-Date: 2021-04-14 19:31-0300
Last-Translator: Pedro Lucas Porcellis <porcellis@eletrotupi.com>
Language-Team: Brazilian Portuguese
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.4.1
 %02i:%02i:%02i Transferência Cancelada %02i:%02i:%02i Erro durante a transferência %02i:%02i:%02i Transferência Concluída %02i:%02i:%02i Baixando… Transferências do Badwolf Favoritos: Feito.
 Favoritos: Encontrado %d favoritos.
 Favoritos: Nenhum arquivo carregável encontrado em %s
 Favoritos: carregando em %s
 Favoritos: não foi possível criar um novo contexto do XPath
 Favoritos: não foi possivel compreender a expressão do XPath "%s"
 Favoritos: não foi possível ler o arquivo "%s"
 Versão do WebKit %d.%d.%d
 Continuar Não foi possível verificar o certificado TLS para garantir uma melhor segurança da conexão. Talvez você deva verificar a sua máquina e rede.

 Erro Transferência Iniciada… Erro: Algum erro desconhecido ocorreu enquanto validava o certificado.
 Erro: A Autoridade de Certificados X509 é desconhecida.
 Erro: O certificado foi revogado.
 Erro: O certificado foi expirado. Verifique o relógio do seu sistema.
 Erro: O certificado é considerado inseguro.
 Erro: O certificado não é válido ainda. Verifique o relógio do seu sistema.
 Erro: A identidade não confere com a esperada.
 Nova aba Abrir uma Nova aba Sem memória Rodando versão %s do Badwolf
 Versão do WebKit: %d.%d.%d
 Erro TLS para %s. Adicionar exceção temporária Habilitar Javascript Habilitar carregamento de imagens automático Erro desconhecido _IMG _JS badwolf: carregado content-filter, adicionando ao content-manager...
 badwolf: falha ao compilar 'content-filters.json', erro: [%d] %s
 badwolf: falha ao carregar content-filter, erro: [%d] %s
 arquivo de content-filters configurado para: %s
 buscar na página atual o processo web travou.
 o processo web excedeu o limite de memória.
 o processo web terminou por uma razão desconhecida.
 diretório de extensões configurado para: %s
 