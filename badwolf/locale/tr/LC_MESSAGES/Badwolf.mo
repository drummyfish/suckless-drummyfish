��    /      �  C           !        ;      Y     z     �     �     �  (   �       .     4   N  %   �  #   �     �  �   �     ^     f  ?   {  2   �  )   �  ?     5   X  C   �  :   �  _        m     u     �     �  !   �     �     �     �  #   	     1	     ?	     D	  =   H	  >   �	  5   �	      �	     
     3
  +   M
  2   y
  *   �
  u  �
  $   M     r  #   �     �     �     �      �  9     %   Y  2     3   �  /   �  +        B  �   K     �     �  =     +   @  .   l  J   �  6   �  O     0   m  n   �  
             '  !   7  .   Y     �     �     �  '   �     �            B     9   Y  4   �  .   �     �       (   #  .   L  1   {        (                
   )   %   -       /                                	          ,   #                 $             "            !              +                           .                   *             &                     '    %02i:%02i:%02i Download cancelled %02i:%02i:%02i Download error %02i:%02i:%02i Download finished %02i:%02i:%02i Downloading… Badwolf Downloads Bookmarks: Done.
 Bookmarks: Found %d bookmarks.
 Bookmarks: No loadable file found at %s
 Bookmarks: loading at %s
 Bookmarks: unable to create new XPath context
 Bookmarks: unable to evaluate XPath expression "%s"
 Bookmarks: unable to parse file "%s"
 Buildtime WebKit version: %d.%d.%d
 Continue Couldn't verify the TLS certificate to ensure a better security of the connection. You might want to verify your machine and network.

 Crashed Download starting… Error: Some unknown error occurred validating the certificate.
 Error: The X509 Certificate Authority is unknown.
 Error: The certificate has been revoked.
 Error: The certificate has expired. Check your system's clock.
 Error: The certificate is considered to be insecure.
 Error: The certificate isn't valid yet. Check your system's clock.
 Error: The given identity doesn't match the expected one.
 Minimalist and privacy-oriented web browser based on WebKitGTK
Runtime WebKit version: %d.%d.%d New tab Open new tab Out of Memory Running Badwolf version: %s
 Runtime WebKit version: %d.%d.%d
 TLS Error for %s. Temporarily Add Exception Toggle javascript Toggle loading images automatically Unknown Crash _IMG _JS badwolf: content-filter loaded, adding to content-manager…
 badwolf: failed to compile content-filters.json, err: [%d] %s
 badwolf: failed to load content-filter, err: [%d] %s
 content-filters file set to: %s
 search in current page the web process crashed.
 the web process exceeded the memory limit.
 the web process terminated for an unknown reason.
 webkit-web-extension directory set to: %s
 Project-Id-Version: Badwolf 1.0.2+g17b9802.develop
Report-Msgid-Bugs-To: contact+badwolf-msgid@hacktivis.me
PO-Revision-Date: 2021-04-10 20:38+0300
Last-Translator: Oğuz Ersen <oguzersen@protonmail.com>
Language-Team: Turkish
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 %02i:%02i:%02i İndirme iptal edildi %02i:%02i:%02i İndirme hatası %02i:%02i:%02i İndirme tamamlandı %02i:%02i:%02i İndiriliyor… Badwolf İndirmeleri Yer imleri: Bitti.
 Yer imleri: %d yer imi bulundu.
 Yer imleri: %s konumunda yüklenebilir dosya bulunamadı
 Yer imleri: %s konumunda yükleniyor
 Yer imleri: yeni XPath içeriği oluşturulamadı
 Yer imleri: "%s" XPath ifadesi değerlendirilemedi
 Yer imleri: "%s" dosyası ayrıştırılamadı
 Derleme zamanı WebKit sürümü: %d.%d.%d
 Devam et Bağlantının güvenliğini daha iyi sağlamak için TLS sertifikası doğrulanamadı. Makinenizi ve ağınızı doğrulamak isteyebilirsiniz.

 Çöktü İndirme başlatılıyor… Hata: Sertifika doğrulanırken bilinmeyen bir hata oluştu.
 Hata: X509 Sertifika Yetkilisi bilinmiyor.
 Hata: Sertifika yürürlükten kaldırıldı.
 Hata: Sertifikanın süresi doldu. Sisteminizin saatini gözden geçirin.
 Hata: Sertifikanın güvensiz olduğu kabul ediliyor.
 Hata: Sertifika henüz geçerli değil. Sisteminizin saatini gözden geçirin.
 Hata: Verilen kimlik beklenen ile eşleşmiyor.
 WebKitGTK tabanlı sadelik ve gizlilik odaklı web tarayıcısı
Çalışma zamanı WebKit sürümü: %d.%d.%d Yeni sekme Yeni sekme aç Yetersiz Bellek Çalışan Badwolf sürümü: %s
 Çalışma zamanı WebKit sürümü: %d.%d.%d
 %s için TLS Hatası. Geçici Olarak İstisna Ekle Javascript'i aç/kapat Resimleri otomatik yüklemeyi aç/kapat Bilinmeyen Çökme _RSM _JS badwolf: content-filter yüklendi, content-manager'e ekleniyor…
 badwolf: content-filters.json derlenemedi, hata: [%d] %s
 badwolf: content-filter yüklenemedi, hata: [%d] %s
 content-filters dosyası şuna ayarlandı: %s
 geçerli sayfada ara web işlemi çöktü.
 web işlemi bellek sınırını aştı.
 web işlemi bilinmeyen bir nedenle sona erdi.
 webkit-web-extension dizini şuna ayarlandı: %s
 