��    /      �  C           !        ;      Y     z     �     �     �  (   �       .     4   N  %   �  #   �     �  �   �     ^     f  ?   {  2   �  )   �  ?     5   X  C   �  :   �  _        m     u     �     �  !   �     �     �     �  #   	     1	     ?	     D	  =   H	  >   �	  5   �	      �	     
     3
  +   M
  2   y
  *   �
  v  �
  '   N  "   v     �     �  &   �     �  .     E   @      �  4   �  9   �  0     :   G     �  �   �     9     I  c   g  J   �  1     _   H  ;   �  e   �  U   J  �   �  	   4     >     M  &   _  :   �     �     �     �  $   
  #   /     S     X  \   \  H   �  B     E   E     �  #   �  6   �  E     H   J        (                
   )   %   -       /                                	          ,   #                 $             "            !              +                           .                   *             &                     '    %02i:%02i:%02i Download cancelled %02i:%02i:%02i Download error %02i:%02i:%02i Download finished %02i:%02i:%02i Downloading… Badwolf Downloads Bookmarks: Done.
 Bookmarks: Found %d bookmarks.
 Bookmarks: No loadable file found at %s
 Bookmarks: loading at %s
 Bookmarks: unable to create new XPath context
 Bookmarks: unable to evaluate XPath expression "%s"
 Bookmarks: unable to parse file "%s"
 Buildtime WebKit version: %d.%d.%d
 Continue Couldn't verify the TLS certificate to ensure a better security of the connection. You might want to verify your machine and network.

 Crashed Download starting… Error: Some unknown error occurred validating the certificate.
 Error: The X509 Certificate Authority is unknown.
 Error: The certificate has been revoked.
 Error: The certificate has expired. Check your system's clock.
 Error: The certificate is considered to be insecure.
 Error: The certificate isn't valid yet. Check your system's clock.
 Error: The given identity doesn't match the expected one.
 Minimalist and privacy-oriented web browser based on WebKitGTK
Runtime WebKit version: %d.%d.%d New tab Open new tab Out of Memory Running Badwolf version: %s
 Runtime WebKit version: %d.%d.%d
 TLS Error for %s. Temporarily Add Exception Toggle javascript Toggle loading images automatically Unknown Crash _IMG _JS badwolf: content-filter loaded, adding to content-manager…
 badwolf: failed to compile content-filters.json, err: [%d] %s
 badwolf: failed to load content-filter, err: [%d] %s
 content-filters file set to: %s
 search in current page the web process crashed.
 the web process exceeded the memory limit.
 the web process terminated for an unknown reason.
 webkit-web-extension directory set to: %s
 Project-Id-Version: Badwolf 1.0.3+gedbbb27.develop
Report-Msgid-Bugs-To: contact+badwolf-msgid@hacktivis.me
PO-Revision-Date: 2021-09-29 10:51+0700
Last-Translator: Ngô Ngọc Đức Huy <huyngo@disroot.org>
Language-Team: Vietnamese
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
 %02i:%02i:%02i Đã huỷ tải xuống %02i:%02i:%02i Lỗi tải xuống %02i:%02i:%02i Đã tải xong %02i:%02i:%02i Đang tải… Các tệp tin đã tải từ Badwolf Dấu trang: Xong.
 Dấu trang: Đã tìm thấy %d dấu trang.
 Dấu trang: Không có tệp tin nào được tìm thấy tại %s
 Dấu trang: đang tải ở %s
 Dấu trang: không thể tạo context XPath mới
 Dấu trang: không thể tính biểu thức XPath "%s"
 Dấu trang: không thể đọc tệp tin "%s"
 Phiên bản WebKit tại thời điểm dựng: %d.%d.%d
 Tiếp tục Không thể xác thực chứng thực TLS để đảm bảo tính bảo mật của kết nối này.Bạn có thể muống xác thực máy của bạn và mạng.
 Gặp sự cố Bắt đầu tải xuống… Lỗi: Có một lỗi không xác định đã xảy ra khi đang hợp lệ hoá chứng thực.
 Lỗi: Nhà cung cấp Chứng thực X509 không được biết đến.
 Lỗi: Chứng thực đã bị vô hiệu hoá.
 Lỗi: Chứng thực đã hết hạn. Hãy kiểm tra đồng hồ hệ thống của bạn.
 Lỗi: Chứng thực được cho là không bảo mật.
 Lỗi: Chứng thực chưa có hiệu lực. Hãy kiểm tra đồng hồ hệ thống của bạn.
 Lỗi: Danh tính nhận được không trùng khớp với danh tính mong đợi.
 Trình duyệt web tối giản và hướng đến sự riêng tư dựa trên WebKitGTK
Phiên bản WebKit tại thời điểm chạy: %d.%d.%d Tab mới Mở tab mới Đầy bộ nhớ Phiên bản Badwolf đang chạy: %s
 Phiên bản WebKit tại thời điểm chạy: %d.%d.%d
 Lỗi TLS cho %s. Tạm thời thêm ngoại lệ Bật/tắt javascript Bật/tắt tự động tải ảnh Gặp sự cố không xác định _IMG _JS badwolf: đã tải bộ lọc nội dung, đang thêm vào trình quản lý nội dung…
 badwolf: biên dịch content-filters.json thất bại, lỗi: [%d] %s
 badwolf: tải bộ lọc nội dung thất bại, lỗi: [%d] %s
 tệp tin chứa bộ lọc nội dung đã được cài thành: %s
 tìm trong trang hiện tại tiến trình web gặp sự cố.
 tiến trình web dùng quá giới hạn bộ nhớ.
 tiến trình web đã kết thúc với lý do không xác định.
 thư mục chứa tiện ích mở rộng webkit đực cài thành: %s
 