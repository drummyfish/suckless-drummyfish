��    (      \  5   �      p  !   q     �      �     �     �            (   4     ]  %   w     �  �   �     .     6  ?   K  ?   �  5   �  C     :   E  _   �     �     �     �       !         B     T     n  #   �     �     �     �  >   �      �          2  +   L  2   x     �  �  �  (   2
  +   [
  %   �
     �
     �
     �
  '   �
  3   '     [  8   t     �  �   �     g     t  U   �  q   �  4   P  y   �  I   �  w   I     �     �  F   �  !   *  &   L     s     �     �  3   �     �             K   	  *   U     �      �  <   �  7   �     5                                                 &   $         (   %              	       !                  "      
       '                                                        #    %02i:%02i:%02i Download cancelled %02i:%02i:%02i Download error %02i:%02i:%02i Download finished %02i:%02i:%02i Downloading… Badwolf Downloads Bookmarks: Done.
 Bookmarks: Found %d bookmarks.
 Bookmarks: No loadable file found at %s
 Bookmarks: loading at %s
 Bookmarks: unable to parse file "%s"
 Continue Couldn't verify the TLS certificate to ensure a better security of the connection. You might want to verify your machine and network.

 Crashed Download starting… Error: Some unknown error occurred validating the certificate.
 Error: The certificate has expired. Check your system's clock.
 Error: The certificate is considered to be insecure.
 Error: The certificate isn't valid yet. Check your system's clock.
 Error: The given identity doesn't match the expected one.
 Minimalist and privacy-oriented web browser based on WebKitGTK
Runtime WebKit version: %d.%d.%d New tab Open new tab Out of Memory Running Badwolf version: %s
 Runtime WebKit version: %d.%d.%d
 TLS Error for %s. Temporarily Add Exception Toggle javascript Toggle loading images automatically Unknown Crash _IMG _JS badwolf: failed to compile content-filters.json, err: [%d] %s
 content-filters file set to: %s
 search in current page the web process crashed.
 the web process exceeded the memory limit.
 the web process terminated for an unknown reason.
 ø Project-Id-Version: Badwolf 1.0.3+g1b10b33.develop
Report-Msgid-Bugs-To: contact+badwolf-msgid@hacktivis.me
PO-Revision-Date: 2021-04-11 14:32+0200
Last-Translator: Ben K. <ben-k@tutanota.com>
Language-Team: German
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.4.2
 %02i:%02i:%02i Herunterladen abgebrochen %02i:%02i:%02i Herunterladen fehlgeschlagen %02i:%02i:%02i Fertig Heruntergeladen %02i:%02i:%02i Lade herunter… Heruntergeladene Dateien Lesezeichen: Erledigt. 
 Lesezeichen: %d Lesezeichen gefunden. 
 Lesezeichen: Keine ladbare Datei unter %s gefunden
 Lesezeichen: %s geladen
 Lesezeichen: Die Datei "%s" konnte nicht geparst werden
 Weiter Das TLS-Zertifikat konnte nicht verifiziert werden, um eine bessere Sicherheit der Verbindung sicherzustellen. Sie möchten vielleicht ihr Gerät und ihr Netzwerk überprüfen. 
 Abgestürtzt Download startet… Fehler: Es ist ein unbekannter Fehler beim überprüfen des Zertifikats aufgetreten.
 Fehler: Das Zertifikat ist abgelaufen. Bitte überprüfen Sie, ob die Uhr ihres Systems richtig eingestellt ist.
 Fehler: Das Zertifikat wird als unsicher angesehen.
 Fehler: Das Zertifikat ist noch nicht gültig. Bitte überprüfen Sie, ob die Uhr ihres Systems richtig eingestellt ist.
 Fehler: Die gegebene Identität passt nicht mit der Erwarteten zusammen.
 Minimalistischer und Privatsphäre-orientierter Internetbrowser basierend of WebKitGTK
Runtime WebKit version: %d.%d.%d Neuer Reiter Neuen Reiter öffnen Ihnen scheint der Arbeitsspeicher ausgegangen zu sein! (Out of Memory) Ausführende Badwolf-Version: %s
 Ausführende WebKit-Version: %d.%d.%d
 TLS-Fehler für %s. Eine Ausnahme machen JavaScript ein-/ausschalten Das automatische Laden von Bildern ein-/ausschalten Unbekannter Absturz _IMG _JS badwolf: kompilieren von content-filters.json fehlgeschlagen, err: [%d] %s
 Inhaltsfilter-Datei wurde gesetzt auf: %s
 Die aktuelle Seite durchsuchen Der Webprozess ist abgestürzt.
 Der Webprozess hat das Arbeitsspeicherlimit überschritten.
 Der Webprozess wurde aus unbekannten Gründen beendet.
 ø 